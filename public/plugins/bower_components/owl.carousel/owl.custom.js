jQuery(document).ready(function($) {
                //owl carousel
      $("#owl-demo").owlCarousel({
     		autoplay:true,
          navigation : true, // Show next and prev buttons
          slideSpeed : 30,
          paginationSpeed : 40,
          //singleItem:true
     
          // "singleItem:true" is a shortcut for:
           items : 1,
          itemsDesktop : true,
          itemsDesktopSmall : true,
          itemsTablet: true,
          itemsMobile : true
     
      });
                $('#owl-demo2').owlCarousel({
				    margin:20,
				    nav:true,
					autoplay:true,
				    responsive:{
				        0:{
				            items:1
				        },
				        480:{
				            items:2
				        },
				        700:{
				            items:4
				        },
				        1000:{
				            items:3
				        },
				        1100:{
				            items:5
				        }
				    }
				})
            });